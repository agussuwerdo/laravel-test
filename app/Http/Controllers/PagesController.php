<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Return Home Page.
     *
     * @return View
     */
    public function home()
    {
        return view('index');
    }
    /**
     * Return About Page.
     *
     * @return View
     */
    public function about()
    {
        $data = array(
            'nama' => 'Agus Suwerdo S.KOM'
        );
        return view('about',$data);
    }
}
