<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();

        return view('students/index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $student = new Student;
        // $student->nama = $request->nama;
        // $student->nrp = $request->nrp;
        // $student->email = $request->email;
        // $student->jurusan = $request->jurusan;
        // $student->is_active = 1;
        // $student->save();

        $validatedData = $request->validate([
            'nama' => 'required|unique:students,nama|max:100',
            'nrp' => 'required|max:10',
            'email' => 'required|unique:students,email|max:50',
            'jurusan' => 'required|max:25',
        ]);

        $newdata = array();
        $newdata = $request->all();
        $newdata['is_active'] = 1;

        Student::create($newdata);

        return redirect('students')->with('status','Data mahasiswa berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('students/show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('students/edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:100',
            'nrp' => 'required|max:10',
            'email' => 'required|max:50',
            'jurusan' => 'required|max:25',
        ]);

        $newdata = array();
        $newdata['nama'] = $request->nama;
        $newdata['email'] = $request->email;
        $newdata['nrp'] = $request->nrp;
        $newdata['jurusan'] = $request->jurusan;
        Student::where('id',$student->id)->update($newdata);

        return redirect('/students')->with('status','Data mahasiswa berhasil di update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student::destroy($student->id);

        return redirect('/students')->with('status','Data berhasil dihapus');
    }
}
