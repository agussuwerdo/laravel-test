@extends('layout/main')
@section('title','Form Ubah Data Mahasiswa')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1>Form Ubah Data Mahasiswa </h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ url('/students') }}/{{ $student->id }}" autocomplete="off">
            @method('put')
            @csrf
                <div class="form-group">
                    <label for="nama">Name</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama" value="{{ $student->nama }}">
                </div>
                <div class="form-group">
                    <label for="nama">NRP</label>
                    <input type="text" class="form-control" id="nrp" name="nrp" placeholder="Masukan NRP" value="{{ $student->nrp }}">
                </div>
                <div class="form-group">
                    <label for="nama">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Masukan email" value="{{ $student->email }}">
                </div>
                <div class="form-group">
                    <label for="nama">Jurusan</label>
                    <input type="text" class="form-control" id="jurusan" name="jurusan" placeholder="Masukan jurusan" value="{{ $student->jurusan }}">
                </div>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
                <a href="{{ url('/students') }}/{{ $student->id }}" class="btn btn-warning ">Kembali</a>
            </form>
        </div>
    </div>
</div>
@endsection
