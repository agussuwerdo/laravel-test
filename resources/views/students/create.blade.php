@extends('layout/main')
@section('title','Form Tambah Data Mahasiswa')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1>Form Tambah Data Mahasiswa </h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ url('/students') }}" autocomplete="off">
            @csrf
                <div class="form-group">
                    <label for="nama">Name</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama" value="{{ old('nama') }}">
                </div>
                <div class="form-group">
                    <label for="nama">NRP</label>
                    <input type="text" class="form-control" id="nrp" name="nrp" placeholder="Masukan NRP" value="{{ old('nrp') }}">
                </div>
                <div class="form-group">
                    <label for="nama">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Masukan email" value="{{ old('email') }}">
                </div>
                <div class="form-group">
                    <label for="nama">Jurusan</label>
                    <input type="text" class="form-control" id="jurusan" name="jurusan" placeholder="Masukan jurusan" value="{{ old('jurusan') }}">
                </div>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
                <a href="{{ url('/students') }}" class="btn btn-warning ">Kembali</a>
            </form>
        </div>
    </div>
</div>
@endsection
