@extends('layout/main')
@section('title','Detail Mahasiswa')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1>Detail mahasiswa</h1>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ $student->nama }}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $student->nrp }}</h6>
                    <p class="card-text">Jurusan : {{ $student->jurusan }}</p>
                    <p class="card-text">Email : {{ $student->email }}</p>
                    <a type="button" href="{{ url('/students') }}/{{ $student->id }}/edit" class="btn btn-primary">Edit</a>
                    <form action="{{ url('/students') }}/{{ $student->id }}" method="post" class="d-inline">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    <a href="{{ url('/students') }}" class="btn btn-warning">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
