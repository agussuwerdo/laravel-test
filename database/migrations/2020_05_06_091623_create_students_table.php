<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->char('nrp', 10)->unique();
            $table->string('email')->unique();
            $table->string('jurusan');
            $table->smallInteger('is_active');
            $table->timestamps();
        });
        $mytime = Carbon\Carbon::now();
        DB::table('students')->insert(
            [
                ['nama'=>'murid nakal',
                'nrp'=>'0615124003',
                'email'=>'muridnakal@gmail.com',
                'jurusan'=>'AB',
                'created_at'=>$mytime->toDateTimeString(),
                'is_active'=>1],
                ['nama'=>'murid baik',
                'nrp'=>'0615124004',
                'email'=>'muridbaik@gmail.com',
                'jurusan'=>'IT',
                'created_at'=>$mytime->toDateTimeString(),
                'is_active'=>1],
                ['nama'=>'murid drop out',
                'nrp'=>'0615124001',
                'email'=>'muriddo@gmail.com',
                'jurusan'=>'AK',
                'created_at'=>$mytime->toDateTimeString(),
                'is_active'=>0]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
